lazy val root = (project in file(".")).
  settings(
    name := "spark",
    version := "1.0",
    scalaVersion := "2.11.11",
    mainClass in Compile := Some("com.andrew_ya.streaming.Main")
  )




val sparkVersion = "2.1.1"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion,
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "com.twitter" % "bijection-avro_2.11" % "0.9.5",
  "org.apache.kafka" % "kafka_2.11" % "0.10.2.1",
  "com.databricks" %% "spark-avro" % "3.2.0",
  "com.typesafe" % "config" % "1.3.1"

)

assemblyMergeStrategy in assembly := {
  case PathList("org", "apache", xs@_*) => MergeStrategy.last
  case PathList("org", "aopalliance", xs@_*) => MergeStrategy.last
  case PathList("javax", "inject", xs@_*) => MergeStrategy.last
  case "overview.html" => MergeStrategy.last
  case x => (assemblyMergeStrategy in assembly).value(x)
}