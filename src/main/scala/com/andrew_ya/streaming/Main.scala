package com.andrew_ya.streaming


import com.andrew_ya.streaming.Avro._
import org.apache.avro.generic.GenericRecord
import org.apache.avro.util.Utf8
import org.apache.kafka.common.serialization.ByteArrayDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark._
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming._

import scala.collection.JavaConverters._
import com.databricks.spark.avro._
import com.typesafe.config.ConfigFactory
import org.apache.log4j.LogManager



object Main {

  val config = ConfigFactory.load

  val conf = new SparkConf().setMaster("local[2]").setAppName("severstal")
  val ssc = new StreamingContext(conf, Seconds(1))

  val spark = SparkSession
    .builder
    .master("local[2]")
    .appName("severstal")
    .getOrCreate()

  import spark.implicits._



  val kafkaParams = Map[String, Object](
    "bootstrap.servers" -> config.getConfig("spark.kafka.consumer").getString("bootstrap-servers"),
    "key.deserializer" -> classOf[ByteArrayDeserializer],
    "value.deserializer" -> classOf[ByteArrayDeserializer],
    "group.id" -> config.getConfig("spark.kafka.consumer").getString("group-id"),
    "auto.offset.reset" -> "latest"
  )

  val topics = Array("test")

  val statsLogger = LogManager.getLogger("statsLogger")


  def main(args: Array[String]): Unit = {
    val stream = KafkaUtils.createDirectStream[Array[Byte], Array[Byte]](
      ssc,
      PreferConsistent,
      Subscribe[Array[Byte], Array[Byte]](topics, kafkaParams)
    )

    val mappedStream = stream.map(d => {
      val record: GenericRecord = recordInjection.invert(d.value).get

      Event(record.get("id").asInstanceOf[Utf8].toString, record.get("data").asInstanceOf[Utf8].toString,
        record.get("timestamp").asInstanceOf[Long],
        Option(record.get("description").asInstanceOf[Utf8]).map(_.toString),
        record.get("arr").asInstanceOf[java.util.List[Int]].asScala.toList)

    })


    mappedStream.window(Seconds(30), Seconds(30)).foreachRDD(rdd => {

      statsLogger.info(s"recieved ${rdd.count} messages")

      val eventDF = rdd.toDF

      eventDF.coalesce(1).write.mode("append").json("data/json")
      eventDF.coalesce(1).write.mode("append").parquet("data/parquet")
      eventDF.coalesce(1).write.mode("append").option("avroSchema", schema.toString).avro("data/avro")


    })

    ssc.start()
    ssc.awaitTermination()
  }


}
