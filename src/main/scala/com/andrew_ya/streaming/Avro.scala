package com.andrew_ya.streaming

import com.twitter.bijection.Injection
import com.twitter.bijection.avro.GenericAvroCodecs
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.avro.specific.SpecificDatumReader

object Avro {

  val schemaString = """
                       |{
                       |    "namespace": "kafka-avro.severstal",
                       |     "type": "record",
                       |     "name": "event",
                       |     "fields":[
                       |         {  "name": "id", "type": "string"},
                       |         {   "name": "data",  "type": "string"},
                       |         {   "name": "timestamp",  "type": "long"},
                       |         {   "name": "description", "type": ["string", "null"]},
                       |         {   "name": "arr", "type": {"type": "array", "items": "int"}}
                       |     ]
                       |}""".stripMargin

  val schema: Schema = new Schema.Parser().parse(schemaString)

  val reader = new SpecificDatumReader[GenericRecord](schema)

  @transient lazy val recordInjection: Injection[GenericRecord, Array[Byte]] = GenericAvroCodecs.toBinary(schema)

}

case class Event(id: String, data: String, timestamp: Long, description: Option[String], array: Seq[Int])